<?php

/*
 * Maximum number of Google Language API requests per transaction
 * Used in automated strings translation section (build/translate/google)
 */
define('MAX_GOOGLE_REQ_PER_TRANSACTION', 400);

/**
 * Implementation of hook_menu_alter().
 **/
function batch_translator_menu_alter(&$items) {
  $items['admin/build/translate/google'] = array(
    'title' => 'Batch/Machine',
    'page callback' => 'locale_inc_callback',
    'page arguments' => array('drupal_get_form', 'batch_translator_translate_google_form'),
    'access arguments' => array('translate interface'),
    'weight' => 60,
    'type' => MENU_LOCAL_TASK,
  );
}

/**
 * User interface for the google strings translation screen.
 */
function batch_translator_translate_google_form() {
    $query = _locale_translate_seek_query();
    
    // Get all languages, except English
    $languages = locale_language_list('name', TRUE);
    unset($languages['en']);

	$form = array();
	$form['filter'] = array('#type' => 'fieldset',
	  '#title' => t('Automatic machine translation using Google'),
	);
	$form['filter']['language'] = array(
	  // Change type of form widget if more the 5 options will
	  // be present (2 of the options are added below).
	  '#type' => (count($languages) <= 3 ? 'radios' : 'select'),
	  '#title' => t('Translate to'),
	  '#required' => true,
	  '#default_value' => (!empty($query['language']) ? $query['language'] : 'all'),
	  '#options' => array_merge($languages, array('all' => 'Every language'))
	);
	$form['filter']['translation'] = array('#type' => 'radios',
	  '#title' => t('Update'),
	  '#default_value' => (!empty($query['translation']) ? $query['translation'] : 'translated'),
	  '#options' => array('translated' => t('Only translated strings'), 'untranslated' => t('Only untranslated strings')),
	);
	$groups = module_invoke_all('locale', 'groups');
	$form['filter']['group'] = array('#type' => 'radios',
	  '#title' => t('Text groups'),
	  '#default_value' => (!empty($query['group']) ? $query['group'] : 'all'),
	  '#options' => array_merge(array('all' => t('All text groups')), $groups),
	);
	
	$form['filter']['submit'] = array('#type' => 'submit', '#value' => t('Translate'));
	$form['#redirect'] = FALSE;
	
	return $form;
}

function batch_translator_translate_google_form_submit($form, &$form_state) {
    $count = _batch_translator_google_batch_translation();
    drupal_set_message(t('Successfully translated %count strings!', array('%count' => $count)));
    return '';
}

function batch_translator_help($path, $arg) {
    if ($path == 'admin/build/translate/google') {
    	return '<p>'. t('Use google translation services for batch strings translation') .'</p>';
	} else {
		return '';
	}
}

/**
 * Perform a string translation and display results in a table
 */
function _batch_translator_google_batch_translation() {
    // We have at least one criterion to match
    if ($query = _locale_translate_seek_query()) {
        $arguments = array();
        $arguments[] = $_REQUEST['language'];    
        $join = "SELECT s.source, s.location, s.lid, s.textgroup, t.translation, t.language AS lang FROM {locales_source} s LEFT JOIN {locales_target} t ON s.lid = t.lid AND t.language LIKE '%s' HAVING lang IS NULL ";
        $count_sql = "SELECT count(*) FROM {locales_source} s LEFT JOIN {locales_target} t ON s.lid = t.lid AND t.language LIKE '%s'";
        $limit_language = FALSE;
        
        $grouplimit = '';
        if (!empty($query['group']) && $query['group'] != 'all') {
            $grouplimit = " AND s.textgroup = '%s'";
            $arguments[] = $query['group'];
        }
        $sql = "$join $grouplimit";
        $result = pager_query($sql, MAX_GOOGLE_REQ_PER_TRANSACTION, 0, $count_sql, $arguments);

        // TODO: $_REQUEST['translation']
                
        $count = 0;
        $report = array();
    
		// s.source, s.location, s.lid, s.textgroup, t.translation, t.language
		while ($locale = db_fetch_object($result)) {
			$count++;
			if ($locale->lang != $_REQUEST['language']) { 
				$translation = translation_framework_translate_send($locale->source, 'en', $_REQUEST['language']);
				_locale_import_one_string_db($report, $_REQUEST['language'], $locale->source,
				$translation, $locale->textgroup, $locale->location, LOCALE_IMPORT_OVERWRITE);              
			}
		}
		return $count;
    }
    return 0;
}